	var mongoose = require('mongoose');
	var conn = mongoose.createConnection('mongodb://127.0.0.1/assign2');
	var uniqueValidator = require('mongoose-unique-validator');	

    var users_schema = mongoose.Schema({
        fname: {
            type: String,
            required: true
        },
        lname: {
            type: String,
            required: true
        },
        email: {
            type: String,
            required: true,
            unique: [true, "email must be unique"]
        },

        username: {
            type: String,
            lowercase: true,
            required: true,
            unique: [true, "email must be unique"],
            trim: true,
            validate: {
                validator: function (v) {
                    if (!v == '') {
                        return true;
                    } else {
                        res.json({
                            message: 'Username should not be empty'
                        });
                        return false;
                    }
                },
                message: 'Username should not be.....'
            },

        },
        pass: {
            type: String,
            required: true,
        },
	}, 
	{

        strict: false,
        collection: 'users'
    });

	users_schema.plugin(uniqueValidator, { type: 'mongoose-unique-validator' });

module.exports = conn.model( "users",users_schema)