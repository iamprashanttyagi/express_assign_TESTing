	var mongoose = require('mongoose');
	var uniqueValidator = require('mongoose-unique-validator');


    var users_schema = mongoose.Schema({
        fname: {
            type: String,
            required: true
        },
        lname: {
            type: String,
            required: true
        },
        email: {
            type: String,
            required: true,
            unique: true
        },

        username: {
            type: String,
            lowercase: true,
            required: true,
            unique: true,
            trim: true,
            validate: {
                validator: function (v) {
                    if (!username == '') {
                        return true;
                    } else {
                        res.json({
                            message: 'Username should not be empty'
                        });
                        return false;
                    }
                },
                message: 'Username should not be.....'
            },

        },
        pass: {
            type: String,
            required: true,
            validate: {
                validator: function (v) {
                    if (pass == conpass) {
                        return true;
                    } else {
                        res.json({
                            message: 'Password are not matched..'
                        });
                        return false;
                    }
                },
                message: 'Password are not matched.....',
            }
        },
    }, {

        strict: false,
        collection: 'users'
    });

    users_schema.plugin(uniqueValidator);

